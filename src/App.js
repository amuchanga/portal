import React, { Component } from 'react';
import PropTypes from 'prop-types';
import i18n from 'd2-i18n';

import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from './Dashboard/Dashboard';
import SnackbarMessage from './SnackbarMessage';
import { fromUser, fromDashboards, fromControlBar } from './actions';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import logo from './logo.png';
import DropDownMenu from 'material-ui/DropDownMenu';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import './App.css';

class App extends Component {
    componentDidMount() {
        const { store, d2 } = this.context;
        store.dispatch(fromUser.acReceivedUser(d2.currentUser));
        store.dispatch(fromDashboards.tFetchDashboards());
        store.dispatch(fromControlBar.tSetControlBarRows());
    }

    getChildContext() {
        return {
            baseUrl: this.props.baseUrl,
            i18n,
        };
    }
    constructor(props) {
        super(props);
        this.state = { value: 1 };
    }

    handleChange = (event, index, value) => this.setState({ value });

    render() {
        return (
            <div className="app-wrapper">
                <AppBar
                    className="AppBar"
                    title="Regional Health Data Warehouse Portal"
                    iconElementLeft={
                        <img src={logo} className="App-logo" alt="logo" />
                    }
                    iconElementRight={
                        <div>
                            <FlatButton
                                className="menu-btn"
                                href="#"
                                label="Inicio"
                            />
                            <FlatButton
                                className="menu-btn"
                                href="#"
                                label="Relatórios"
                            />
                            <DropDownMenu
                                className="language-dropdown"
                                value={this.state.value}
                                onChange={this.handleChange}
                                anchorOrigin={{
                                    horizontal: 'left',
                                    vertical: 'top',
                                }}
                            >
                                <MenuItem value={1} primaryText="Portugues" />
                                <MenuItem value={2} primaryText="English" />
                                <MenuItem value={3} primaryText="Francais" />
                            </DropDownMenu>
                            <IconMenu
                                iconButtonElement={
                                    <IconButton>
                                        <MoreVertIcon />
                                    </IconButton>
                                }
                                anchorOrigin={{
                                    horizontal: 'left',
                                    vertical: 'top',
                                }}
                                targetOrigin={{
                                    horizontal: 'left',
                                    vertical: 'top',
                                }}
                            >
                                <MenuItem primaryText="Refresh" />
                                <MenuItem primaryText="Send feedback" />
                                <MenuItem primaryText="Settings" />
                                <MenuItem primaryText="Help" />
                                <MenuItem primaryText="Sign out" />
                            </IconMenu>
                        </div>
                    }
                />
                <Router>
                    <Switch>
                        <Route
                            exact
                            path="/"
                            render={props => (
                                <Dashboard {...props} mode="view" />
                            )}
                        />
                        <Route
                            exact
                            path="/:dashboardId"
                            render={props => (
                                <Dashboard {...props} mode="view" />
                            )}
                        />
                    </Switch>
                </Router>
                <SnackbarMessage />
            </div>
        );
    }
}

App.contextTypes = {
    d2: PropTypes.object,
    store: PropTypes.object,
};

App.childContextTypes = {
    baseUrl: PropTypes.string,
    i18n: PropTypes.object,
};

export default App;
